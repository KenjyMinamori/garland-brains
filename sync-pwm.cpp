#include "sync-pwm.h"

SyncPWM::SyncPWM(uint8_t pin, uint32_t per, bool pulse = true) {
  pulseMode = pulse;
  pinName = pin;
  period = per;
  pinMode(pinName, OUTPUT);
}


/*
    Start pwm [0..1000]

*/
void SyncPWM::doPWM(short PWMvalue) {
  isOn = true;
  // validate value
  if (PWMvalue > 999) PWMvalue = 999;
  if (PWMvalue < 0) PWMvalue = 0;
  PWMvalue = 1000 - PWMvalue;
  // Hardcode: min value is the 1/2 of max period to avoid collisions.
  offTime = (period / 2) + ((period * 1 / 2) / 1000 * PWMvalue);
}

void SyncPWM::stopPWM() {
  isOn = false;
  digitalWrite(pinName, 0);
}

void SyncPWM::loop() {
  if (!isOn) return;
  uint32_t now = micros();
  uint32_t delta = now - syncPoint;
  //short offTime = PWMvalue;
  if (delta < offTime) {
    digitalWrite(pinName, 0);
    loopDone = false;
  } else {
    if (!loopDone) {
      digitalWrite(pinName, 1);
      loopDone = true;
      if (pulseMode) digitalWrite(pinName, 0);
    }
  }
}


uint32_t SyncPWM::handleImpulse() {
  uint32_t now = micros();
  uint32_t delta = now - syncPoint;

  if (delta > 1000 ) {
    setSyncPoint(now);
  }
  return syncPoint;
}


void SyncPWM::setSyncPoint(uint32_t val) {
  syncPoint = val;
}

