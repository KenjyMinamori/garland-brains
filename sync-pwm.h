#ifndef SYNC-POWER_H
#define SYNC-POWER_H

#include "Arduino.h"


class PWMPin {
  public:
    PWMPin(uint8_t pinNumber);
    
  protected:
    uint8_t pin;
    uint16_t val;
};


class SyncPWM {
  public:
    SyncPWM(uint8_t pinName, uint32_t period, bool pulseMode);
    void loop();
    uint32_t handleImpulse();
    void setSyncPoint(uint32_t);
    void doPWM(short PWMvalue);
    void stopPWM();
    
  protected:
    uint8_t pinName;
    volatile uint32_t period;
    volatile uint32_t syncPoint;
    bool pulseMode;
    uint32_t loopDone = false;
    uint8_t pinNumbers[];
    short offTime = 0;
    bool isOn = false;
};







#endif //SYNC-POWER_H
