#include <stdint.h>
#include "sync-pwm.h"

#define LED1 11
#define LED2 10
#define LED3 9
#define LED4 6

volatile uint32_t delayer = 1000;
volatile bool     started = true;
volatile uint32_t elapsed = 0;
volatile uint32_t lastSynchro = 0;
volatile int16_t  percents = 0;

volatile uint16_t everySecondPeriod = 50;

SyncPWM line1(LED1, 8500, true);
SyncPWM line2(LED2, 8500, true);
SyncPWM line3(LED3, 8500, true);
SyncPWM line4(LED4, 8500, true);

void handlePwm() {
  uint32_t syncPoint = line1.handleImpulse();
  line2.setSyncPoint(syncPoint);
  line3.setSyncPoint(syncPoint);
  line4.setSyncPoint(syncPoint);
}


void changePercent() {
  static bool up = true;
  static uint8_t step = 100;
  static uint8_t state = 1;
  if (up) {
    percents += step;
    if (percents > 999) {
      percents = 999;
      up = false;
    }
  } else {
    percents -= step;
    if (percents < 1 ) {
      percents = 1;
      up = true;
      state++;
    }
  }
  Serial.print("P: ");
  Serial.print(percents);
  Serial.print(" s: ");
  Serial.print(state);
  if (state > 4) {
    state = 1;
  }
  switch (state) {
    case 1:
      line1.doPWM(percents);
      line2.stopPWM();
      line3.stopPWM();
      line4.stopPWM();
      break;
    case 2:
      line1.stopPWM();
      line2.doPWM(percents);
      line3.stopPWM();
      line4.stopPWM();
      break;
    case 3:
      line1.stopPWM();
      line2.stopPWM();
      line3.doPWM(percents);
      line4.stopPWM();
      break;
    case 4:
      line1.stopPWM();
      line2.stopPWM();
      line3.stopPWM();
      line4.doPWM(percents);
      break;
  }
}

void everySecondEvent(int (*someCode)(void)) {
  static uint32_t last = 0;
  static uint32_t now = 0;
  static uint32_t delta = 0;
  now = millis();
  delta = now - last;
  if (delta > everySecondPeriod) {
    someCode();
    last = now;
  }

}

void setup() {
  Serial.begin(115200);
  attachInterrupt(0, handlePwm, RISING);
  delayer = 8500;
}

void loop() {
  everySecondEvent(&changePercent);
  line1.loop();
  line2.loop();
  line3.loop();
  line4.loop();
  if (Serial.available() > 0) {
    Serial.read();
    percents += 50;
    Serial.println(delayer);
  }
}


